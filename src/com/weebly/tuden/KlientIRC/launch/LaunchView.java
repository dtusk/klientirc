package com.weebly.tuden.KlientIRC.launch;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class LaunchView extends JFrame {

	private JTextField nickTextField, serverTextField, channelTextField;
	private JButton loginButton;

	public LaunchView(LaunchModel model) {
		setTitle("Logowanie - IRC Klient [Dennis Tusk]");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(320, 180);
		setLocationRelativeTo(null);
		setResizable(false);
		setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel(new FlowLayout());
		nickTextField = new JTextField("Login", 20);
		serverTextField = new JTextField("Serwer", 20);
		channelTextField = new JTextField("#kanał", 20);
		loginButton = new JButton("Dołącz");
		JPanel bottomPanel = new JPanel();
		mainPanel.add(nickTextField);
		mainPanel.add(serverTextField);
		mainPanel.add(channelTextField);
		mainPanel.add(loginButton);
		if (!LaunchModel.isInternet()) {
			nickTextField.setEnabled(false);
			serverTextField.setEnabled(false);
			channelTextField.setEnabled(false);
			loginButton.setEnabled(false);
			bottomPanel.add(new JLabel(
					"<html><center><b>Wygląda na to że nie ma internetu!<br>Napraw internet i zrestartuj aplikacje </b></center></html>"),
					BorderLayout.SOUTH);
		}
		getRootPane().setDefaultButton(loginButton);
		add(new JPanel(), BorderLayout.NORTH);
		add(new JPanel(), BorderLayout.EAST);
		add(bottomPanel, BorderLayout.SOUTH);
		add(mainPanel, BorderLayout.CENTER);
		add(new JPanel(), BorderLayout.WEST);
	}

	
	public void addLoginButtonListener(ActionListener listener) {
		loginButton.addActionListener(listener);
	}
	
	public String getNick() {
		return nickTextField.getText();
	}
	
	public void setNick(String nick) {
		nickTextField.setText(nick);
	}

	public String getServer() {
		return serverTextField.getText();
	}
	
	public void setServer(String server) {
		serverTextField.setText(server);
	}
	
	public String getChannel() {
		return channelTextField.getText();
	}
	
	public void setChannel(String t) {
		channelTextField.setText(t);
	}

}
