package com.weebly.tuden.KlientIRC.launch;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.weebly.tuden.KlientIRC.KlientIRCMain;



public class LaunchController {

	private LaunchModel model;
	private LaunchView view;
	private LoginButtonHandler loginButtonHandler;
	String nick, server;
	
	public LaunchController(LaunchModel model, LaunchView view) {
		this.model = model;
		this.view = view;
		nick = view.getNick();
		server = view.getServer();
		loginButtonHandler = new LoginButtonHandler();
		view.addLoginButtonListener(loginButtonHandler);
	}
	
	private class LoginButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String nick = view.getNick(), server = view.getServer(), channel = view.getChannel();
			if (server.equals("") || nick.equals("") || channel.equals("")) {
				view.setVisible(false);
				JOptionPane.showMessageDialog(null, "Pola serwer i/lub nick są puste", "Błąd", JOptionPane.ERROR_MESSAGE);
				view.setVisible(true);
			} else if (server.equals("Serwer") || nick.equals("Nick")) {
				view.setVisible(false);
				JOptionPane.showMessageDialog(null, "Pospolity enter?", "Błąd", JOptionPane.ERROR_MESSAGE);
				view.setVisible(true);
			} else {
				view.setVisible(false);
				if (!model.isServerResponding(server)) {
					JOptionPane.showMessageDialog(null, "Serwer nie odpowiada", "Błąd", JOptionPane.ERROR_MESSAGE);
					view.setVisible(true);
				} else {
					new KlientIRCMain(server, nick, channel);
				}
			}
		}
		
	}


}
