package com.weebly.tuden.KlientIRC;

import com.weebly.tuden.KlientIRC.main.*;

/**
 * Uruchamia główne okienko.
 * @author tuden
 *
 */
public class KlientIRCMain {

	/**
	 * @param nick 
	 * @param server 
	 * @param args
	 */
	public KlientIRCMain(String server, String nick, String channel) {
		FrameModel model = new FrameModel(server, nick, channel);
		FrameView view = new FrameView(model);
		new FrameController(model, view);
		view.setVisible(true);
	}
}
