package com.weebly.tuden.KlientIRC;

import com.weebly.tuden.KlientIRC.launch.LaunchController;
import com.weebly.tuden.KlientIRC.launch.LaunchModel;
import com.weebly.tuden.KlientIRC.launch.LaunchView;

/**
 * Jest głównym wyzwalaczem wzorca MVC. Chciałem bardziej rozbudować tego
 * klienta w dynamiczne taby ale ze względu na sprężony czas nie mogłem bardziej
 * go uatrakcyjnic. Jest to najbardziej podstawowy klient IRC. Można czatować z uzytkownikami,
 * wysłać PM'y. 
 * 
 * Tworzy okienko do logowania na serwer IRC
 * 
 * INFO:
 * 	czas pracy - 11 godzin. (starałem stworzyć dynamiczne taby ale to za długo trwa).
 * 
 * @author tuden
 * 
 */
public class KlientIRC {

	/**
	 * Posiada MVC + ustawia View.setVisible(true)
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LaunchModel lmodel = new LaunchModel();
		LaunchView lview = new LaunchView(lmodel);
		new LaunchController(lmodel, lview);
		lview.setVisible(true);
	}

}
