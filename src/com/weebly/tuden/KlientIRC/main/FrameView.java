package com.weebly.tuden.KlientIRC.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
/**
 * Jedna część z wzorca projektowego MVC (Widok).
 * @author tuden
 *
 */
public class FrameView extends JFrame {

	private JList<String> list;
	private JButton serverWriteButton, channelWriteButton;
	private JTextField serverWriteTextField, channelWriteTextField;
	private final JTabbedPane tabbedPane;
	private JTextArea channelTextArea, serverTextArea;
	private JScrollPane serverScrollPane, channelScrollPane;
	private JScrollBar serverScrollBar, channelScrollBar;

	public FrameView(FrameModel model) {
		tabbedPane = new JTabbedPane();
		serverWriteButton = new JButton("Wyślij");
		serverWriteTextField = new JTextField();
		serverScrollBar = new JScrollBar();
		channelScrollBar = new JScrollBar();
		// ustawienia okna
		setTitle("IRC - Projekt [Dennis Tusk]");
		setSize(640, 480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		// lista
		list = new JList<String>(model.getDefaultListModel());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);

		// Panele
		JPanel serverPanel = new JPanel(new BorderLayout());
		JPanel serverWritePanel = new JPanel(new BorderLayout());
		JPanel channelPanel = new JPanel(new BorderLayout());
		JPanel channelWritePanel = new JPanel(new BorderLayout());

		tabbedPane.addTab("Głowne", serverPanel);
		tabbedPane.addTab("" + model.getChannel(), channelPanel);

		// Output Serwer
		serverTextArea = new JTextArea();
		serverTextArea.setLineWrap(true);
		serverTextArea.setEditable(false);
		serverTextArea.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));

		serverWritePanel.add(serverWriteTextField, BorderLayout.CENTER);
		serverWritePanel.add(serverWriteButton, BorderLayout.EAST);

		serverScrollPane = new JScrollPane(serverTextArea);
		serverScrollPane.setVerticalScrollBar(serverScrollBar);

		serverPanel.add(serverScrollPane, BorderLayout.CENTER);
		serverPanel.add(serverWritePanel, BorderLayout.SOUTH);

		// Output Kanał
		channelTextArea = new JTextArea();
		channelTextArea.setEditable(false);
		channelTextArea.setLineWrap(true);
		channelTextArea.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));

		channelWriteButton = new JButton("Wyślij");
		channelWriteTextField = new JTextField();
		channelWritePanel.add(channelWriteTextField, BorderLayout.CENTER);
		channelWritePanel.add(channelWriteButton, BorderLayout.EAST);

		getRootPane().setDefaultButton(channelWriteButton);

		channelScrollPane = new JScrollPane(channelTextArea);
		channelScrollPane.setVerticalScrollBar(channelScrollBar);

		channelPanel.add(channelScrollPane, BorderLayout.CENTER);
		channelPanel.add(new JScrollPane(list), BorderLayout.EAST);
		channelPanel.add(channelWritePanel, BorderLayout.SOUTH);
		add(tabbedPane, BorderLayout.CENTER);
		channelScrollBar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
			}
		});
		serverScrollBar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
			}
		});
	}

	public void addServerWriteButtonActionListener(ActionListener l) {
		serverWriteButton.addActionListener(l);
	}

	public void addChannelWriteButtonActionListener(ActionListener l) {
		channelWriteButton.addActionListener(l);
	}

	public void addTabbedPaneKeyListener(KeyListener listener) {
		tabbedPane.addKeyListener(listener);
	}

	public void addTabbedPaneChangeListener(ChangeListener listener) {
		tabbedPane.addChangeListener(listener);
	}

	public void addListMouseListener(MouseListener listener) {
		list.addMouseListener(listener);
	}

	public String getServerTextAreaText() {
		return serverTextArea.getText();
	}

	public void setServerTextAreaText(String t) {
		serverTextArea.setText(t);
	}

	public void appendServerTextAreaText(String str) {
		serverTextArea.append(str);
	}

	public void setChannelWriteTextField(String t) {
		channelWriteTextField.setText(t);
	}

	public String getChannelWriteTextField() {
		return channelWriteTextField.getText();
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public JList<String> getJList() {
		return list;
	}

	public void appendChannelTextAreaText(String str) {
		channelTextArea.append(str);
	}

	public void setServerWriteTextField(String string) {
		serverWriteTextField.setText(string);
	}

	public JScrollPane getServerScrollPane() {
		return serverScrollPane;
	}

}
