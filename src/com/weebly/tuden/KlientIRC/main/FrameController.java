package com.weebly.tuden.KlientIRC.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * Glowna czesc z wzorca projektowego MVC (Kontroler).
 * @author tuden
 *
 */
public class FrameController {

	private FrameModel model;
	private JTabbedPane tabbedPane;
	private JList<String> list;
	private final String TITLE;
	private FrameView view;
	private String nick, channel;


	public FrameController(FrameModel model, FrameView view) {
		this.view = view;
		this.model = model;
		nick = model.getNick();
		channel = model.getChannel();
		TITLE = view.getTitle();
		tabbedPane = view.getTabbedPane();
		model.getDefaultListModel();
		list = view.getJList();
		view.addServerWriteButtonActionListener(new ServerWriteButtonHandler());
		view.addChannelWriteButtonActionListener(new ChannelWriteButtonHandler()); // zrobione
		view.addTabbedPaneChangeListener(new TabbedPaneChangeHandler()); // zrobione
		view.addTabbedPaneKeyListener(new TabbedPaneKeyHandler()); // zrpbione
		view.addListMouseListener(new ListMouseHandler()); // TODO
		view.addWindowListener(new WindowHandler()); // zrobione
		if (model.logToServer()) {
			model.joinChannel();
			model.listenServer(view);
			model.checkIfDisconnected();
		} else {
			JOptionPane.showMessageDialog(null,
					"Istnieje juz ten sam nick na serwerze IRC!s", "Błąd",
					JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

	/**
	 * Prywatna klasa ChannelWriteButtonHandler. Obsługuje przycisk dot.
	 * wysylania do kanału
	 * 
	 * @author tuden
	 * 
	 */
	private class ChannelWriteButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String line = view.getChannelWriteTextField();
			if (line.toLowerCase().startsWith("/msg ")) {
				line = line.substring(5);
				String pal = line.split(" ")[0];
				line = line.substring(pal.length() + 1);
				// easter egg :)
				view.appendChannelTextAreaText("(Ty z komputra: " + line
						+ ")\n");
				model.sendMessageToPal(pal, line);
			} else if (line.toLowerCase().startsWith("/part")) {
				model.partChannel(channel);
			} else if (line.toLowerCase().startsWith("/quit")) {
				System.exit(1);
			} else if (line.toLowerCase().startsWith("/help")) {
				view.appendChannelTextAreaText("Dostępne komendy:\n\t/msg [nick] [wiadomosc] - wysylasz prywatna wiadomosc\n\t"
						+ "/part - opuszcza czat\n\t/quit - wylaczasz program\n");
			} else {
				model.sendMessageInChannel(channel, line);
				view.appendChannelTextAreaText(nick + ": " + line + "\n");
			}
			view.setChannelWriteTextField("");
		}
	}
	/**
	 * Prywatna klasa ServerWriteButtonHandler. Obsługuje przycisk dot.
	 * wysylania do serwera IRC
	 * 
	 * @author tuden
	 * 
	 */
	private class ServerWriteButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String line = view.getServerTextAreaText();
			if (line.startsWith("/join #")) {
				String query = line.replace("/join #", "");
				model.joinChannel(query);
			} else if (line.startsWith("/part")) {
				model.partChannel();
			} else if (line.startsWith("/quit")) {
				System.exit(1);
			}
			view.setServerWriteTextField("");
		}

	}

	private class WindowHandler implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {

		}

		@Override
		public void windowClosed(WindowEvent e) {
			model.quit();
		}

		@Override
		public void windowClosing(WindowEvent e) {

		}

		@Override
		public void windowDeactivated(WindowEvent e) {

		}

		@Override
		public void windowDeiconified(WindowEvent e) {

		}

		@Override
		public void windowIconified(WindowEvent e) {

		}

		@Override
		public void windowOpened(WindowEvent e) {

		}

	}

	private class TabbedPaneChangeHandler implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent e) {
			view.setTitle(TITLE + " - "
					+ tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
		}
	}

	private class ListMouseHandler implements MouseListener {
		@Override
		public void mouseReleased(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent e) {

		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseClicked(MouseEvent e) {
			String _nick = list.getSelectedValue();
			if (!_nick.equals(FrameController.this.nick)) {
				if (_nick.contains("@"))
					_nick = _nick.replace("@", "");
				else if (_nick.contains("^"))
					_nick = _nick.replace("^", "");
				// tabbedPane.addTab("Czat z " + _nick,new Chat(view, model); //
				// TODO dynamiczne taby
			}
		}
	}

	private class TabbedPaneKeyHandler implements KeyListener {

		@Override
		public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE
					&& (tabbedPane.getSelectedIndex() != 0 || tabbedPane
							.getSelectedIndex() != 1)) {
				tabbedPane.remove(tabbedPane.getSelectedIndex());
			}
		}
	}

}
