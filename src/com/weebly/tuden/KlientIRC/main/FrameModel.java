package com.weebly.tuden.KlientIRC.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import com.weebly.tuden.KlientIRC.launch.LaunchModel;


public class FrameModel {

	private BufferedWriter writer;
	private static boolean inChat;
	private BufferedReader reader;
	private String server, nick, channel;
	private DefaultListModel<String> list;
	private Socket socket;

	public FrameModel(String server, String nick, String channel) {
		this.server = server;
		this.nick = nick;
		this.channel = channel;
		list = new DefaultListModel<String>();
	}
	
	/**
	 * Loguje się do serwera
	 * @return
	 */
	public boolean logToServer() { // dziala
		try {
			socket = new Socket(server, 6667);
			writer = new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream()));
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			writer.write("NICK " + nick + "\r\n");
			writer.write("USER " + nick + " 8 * : Java IRC\r\n");
			writer.flush();
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.indexOf("004") >= 0) {
					return true;
				} else if (line.indexOf("433") >= 0) {
					System.err.println("Nick jest w użyciu.");
					return false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Piszy do kanalu
	 * @param channel
	 * @param msg
	 */
	public void sendMessageInChannel(String channel, String msg) {
		try {
			writer.write("PRIVMSG " + channel + " :" + msg + "\r\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Dołącza do czatu
	 * @param channel
	 */
	public void joinChannel(String channel) {
		if (!inChat) {
			try {
				writer.write("JOIN " + channel + "\r\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				inChat = true;
			}
		}
	}

	public void joinChannel() {
		if (!inChat) {
			try {
				writer.write("JOIN " + channel + "\r\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				inChat = true;
			}
		} else {
			partChannel();
		}
	}

	
	/**
	 * Słucha serwer IRC
	 * @param view
	 */
	public void listenServer(final FrameView view) {
		// interfejs
		new Thread(new Runnable() {

			@Override
			public void run() {
				String line = null;
				try {
					while ((line = reader.readLine()) != null) {
						if (line.startsWith("PING ")) {
							// Jeżeli nie odpowiemy zostaniemy wyrzuceni z
							// serwera
							writer.write("PONG " + line.substring(5) + "\r\n");
							writer.flush();
						} else if (line.contains("QUIT")
								|| line.contains("PART")) {
							String nick_do_usuniecia = line.substring(1).split(
									"!")[0];
							list.removeElement(nick_do_usuniecia);
							view.appendChannelTextAreaText("(Odszedł "
									+ nick_do_usuniecia + ")\n");
						} else if (line.contains("PRIVMSG " + channel)) {
							String _nick = line.substring(1).split("!")[0];
							String tresc = null;
							for (String _tresc : line.substring(1)
									.split(":", 2))
								tresc = _tresc;
							view.appendChannelTextAreaText(_nick + ": " + tresc
									+ "\n");
						} else if (line.contains("PRIVMSG " + nick)) {
							String _nick = line.substring(1).split("!")[0];
							String tresc = null;
							for (String _tresc : line.substring(1)
									.split(":", 2))
								tresc = _tresc;
							view.appendChannelTextAreaText("(PM " + _nick + ": " + tresc
									+ ")\n");
						} else if (line.contains("JOIN")) {
							String _nick = line.substring(1).split("!")[0];
							if (line.contains(nick))
								continue;
							list.addElement(_nick);
							view.appendChannelTextAreaText("(Dołączył " + _nick
									+ ")\n");
						} else if (line.contains("NICK")) {
							String _nick = line.substring(1).split("!")[0];
							list.removeElement(_nick);
							String tresc = null;
							for (String _tresc : line.substring(1)
									.split(":", 2))
								tresc = _tresc;
							list.addElement(tresc);
						} else if (line.contains(" @ " + channel)) {
							list.removeAllElements();
							String tmp = null;
							for (String _tmp : line.split(" @ " + channel
									+ " :"))
								tmp = _tmp;
							// tmp posiada teraz liste
							// przyklad: tuden neodave jachman
							for (String i : tmp.split(" ")) {
								list.addElement(i);
							}
						} else if (line.contains(" = " + channel)) {
							list.removeAllElements();
							String tmp = null;
							for (String _tmp : line.split(" = " + channel
									+ " :"))
								tmp = _tmp;
							// tmp posiada teraz liste
							// przyklad: tuden neodave jachman
							for (String i : tmp.split(" ")) {
								list.addElement(i);
							}
						}
						view.appendServerTextAreaText(line + "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}
		}).start();
	}

	/**
	 * Odłącza od kanalu
	 */
	public void partChannel() {
		if (!inChat) {
			try {
				writer.write("PART\r\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				inChat = false;
			}
		}
	}

	public void partChannel(String reason) {
		if (!inChat) {
			try {
				writer.write("PART :" + reason + "\r\n");
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				inChat = false;
			}
		}
	}

	public void quit() {
		try {
			writer.write("QUIT\r\n");
			writer.flush();
			System.out.println(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getNick() {
		return nick;
	}

	public String getServer() {
		return server;
	}

	public String getChannel() {
		return channel;
	}

	public DefaultListModel<String> getDefaultListModel() {
		return list;
	}

	/**
	 * Sprawdza czy podczas włączonego klienta IRC jest internet 
	 */
	public void checkIfDisconnected() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true)
					if (!LaunchModel.isInternet())
						break;
				JOptionPane.showMessageDialog(null,
						"Nie ma połączenia z internetem!", "Błąd",
						JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		}).start();
	}
	
	
	/**
	 * Wysyła prywatną wiadomość do korespondenta(?)
	 * @param pal
	 * @param msg
	 */
	public void sendMessageToPal(String pal, String msg) {
		try {
			writer.write("PRIVMSG " + pal + " :" + msg + "\r\n" );
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
